const config = require('./config');
const {Pool} = require('pg');

const pool = new Pool(config);

function runQuery(q, vals) {
	console.log('query', q.replace(/\s+/g, ' '));
	if (vals) console.log('with', vals.length, ( vals.length > 1 ? 'values': 'value'));
	return pool.query(q, vals)
		.then( result => {
			console.log('got', result.rowCount, 'results');
			return result;
		})
		.catch( e => {
		console.log('database error:', e);
		throw 'did you load the env_vars required in config.js?'
		})
	;
}

async function queryFirst(q, vals) {
	let result = await runQuery(q, vals);
	return (result.rows ? result.rows[0] : undefined);
}

async function queryList(q, vals) {
	let result = await runQuery(q, vals);
	return (result.rows ? result.rows : undefined);
}

async function modifyRows(q, vals) {
	let result = await runQuery(q, vals);
	return (result ? (result.rowCount > 0) : false);
}

function getUser(id) {
	return queryFirst(`SELECT * FROM users WHERE id=$1`, [id]);
}

function getUserByName(uname) {
	return queryFirst(`SELECT * FROM users WHERE username=$1`, [uname]);
}

function getPost(id) {
	return queryFirst(
		`SELECT p.*, u.username AS author FROM posts p JOIN users u ON u.id = p.user_id WHERE p.id=$1`
		,[id]
	);
}

function getPosts() {
	return queryList(
		`SELECT p.*, u.username AS author FROM posts p JOIN users u ON u.id = p.user_id ORDER BY p.created_at DESC LIMIT 10`
	);
}

function getPostsForUser(uname) {
	return queryList(
		`SELECT p.*, u.username FROM posts p JOIN users u ON u.id = p.user_id AND u.username = $1 ORDER BY p.created_at DESC LIMIT 10`
		,[uname]
	);
}

function getCode(c) {
	return queryFirst(`SELECT * FROM codes WHERE code=$1`, [c]);
}

function createUser(u, p) {
	return modifyRows(`INSERT INTO users (username, password)
		VALUES ($1, $2)`
		,[u, p]
	);
}

function createCode(code, uid) {
	return modifyRows(
		`INSERT INTO codes (code, created_by) values ($1, $2)`
		,[code, uid]
	);
}

function createPost(uid, title, body) {
	console.log('creating post', title, 'for user', uid);
	return modifyRows(
		`INSERT INTO posts (user_id, title, body) values ($1, $2, $3)`
		,[uid, title, body]
	);
}

function updatePost(pid, uid, title, body) {
	return modifyRows(
		`UPDATE posts SET body = $4, title=$3 WHERE user_id=$2 AND id=$1`
		,[pid, uid, title, body]
	);
}

function deletePost(pid, uid) {
	return modifyRows(
		`DELETE FROM posts WHERE id=$1 AND user_id=$2`
		,[pid, uid]
	);
}

function updateUserProfile(uid, profile) {
	return modifyRows(
		`UPDATE users SET profile = $2 WHERE id = $1`
		,[uid, profile]
	);
}

module.exports = {
	runQuery: runQuery
	,getPost: getPost
	,getPosts: getPosts
	,getUser: getUser
	,getUserByName: getUserByName
	,getPostsForUser: getPostsForUser
	,getCode: getCode
	,createUser: createUser
	,createCode: createCode
	,createPost: createPost
	,updatePost: updatePost
	,updateUserProfile: updateUserProfile
	,deletePost: deletePost
}
