const db = require('./db.js');
const auth = require('./auth.js');
const fs = require('fs');

function validUsername(u) {
	return (u && u.length >= 5);
}

function validPassword(p) {
	return (p && p.length >= 5);
}

function createUser(u, p) {
	return db.getUserByName(u).then( user => {
		if (!user && validUsername(u) && validPassword(p) && u !== p) {
			return auth.hashPassword(p).then( hash => {
				return db.createUser(u, hash);
			});
		} else if (user) {
			return new Error('username taken');
		} else if (!validUsername(u)) {
			throw 'invalid username, must be > 4 characters';
		} else if (!validPassword(p)) {
			throw 'invalid password, must be > 4 characters';
		} else if (u == p ) {
			throw "password cannot be the username, it's bad form";
		} else {
			throw 'error creating user';
		}
	});
}

function createDatabase() {
	var data = fs.readFileSync('init.sql', 'utf8');
	return db.runQuery(data, []);
}

function createTestData() {
	var data =fs.readFileSync('test_data.sql', 'utf8');
	return db.getUser(1).then( result => {
		return db.runQuery(data, []);
	}).catch( e => {
		throw 'no test user, run createuser first';
	});
}

module.exports = {
	createUser: createUser
	,createDatabase: createDatabase
	,createTestData: createTestData
};
