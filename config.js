var config = {};

config.connectionString = process.env.DATABASE_URL || 'postgresql://user:secret@hostname:5432/database';

config.secret = process.env.SECRETKEY || "uh_oh_change_this";
config.session_name = process.env.SESSIONNAME || "wotc_session";
config.tokenName = process.env.TOKENNAME || 'wilboToken';
config.inviteTimeout = process.env.INVITETIMEOUT || 300000;
config.jwt = {
	expiresIn: 10000
};

module.exports = config;
