#!/usr/bin/env node

const readline = require('readline');
const db = require('../db');
const utils = require('../utils');

rl = readline.createInterface({
	input: process.stdin
	,output: process.stdout
 });

function ask(q) {
	return new Promise( resolve => {
		rl.question(q, ans => resolve(ans));
	});
}

async function createUser() {
	let successful = false;
	let u = await ask('enter new username:');
	let p1 = await ask('enter new password:');
	let p2 = await ask('confirm password:');
	return await Promise.all([u, p1, p2]).then( arr => {
		if (arr[0] && arr[1] && arr[2] && arr[1] === arr[2]) {
			return utils.createUser(arr[0], arr[1]);
		} else {
			throw 'bad username/password';
		}
	});
}

function createUserCLI() {
	createUser().then( user => {
		console.log('created user?', user);
		console.log('you can now login and generate invite codes');
	})
	.catch( e => {
		console.log(e);
		console.log('failed to create user');
	})
	.finally( () => {
		rl.close();
		process.exit(0);
	});
}

function createDBCLI() {
	utils.createDatabase().then( res => {
		console.log('created database');
	}).catch( e => {
		console.log(e);
		console.log('error creating database');
	}).finally( () => {
		process.exit(0);
	});
}

function createTestDataCLI() {
	utils.createTestData().then( res => {
		console.log('test data created');
	}).catch( e => {
		console.log(e);
		console.log('error creating test data');
	})
	.finally( () => {
		process.exit(0);
	});
}

function main() {
	if (process.argv.length !== 3) {
		console.log('specify one, and only one, command to run');
		process.exit(1);
	}
	switch(process.argv[2]){
		case 'createdb':
			createDBCLI();
			break;
		case 'createuser':
			createUserCLI();
			break;
		case 'createdata':
			createTestDataCLI();
			break;
		default:
			console.log('invalid command:', process.argv[2]);
			console.log(process.argv[2] === 'createuser');
			process.exit(0);
	}
}
main();
