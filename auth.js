var config = require('./config');
let jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var expJWT = require('express-jwt');
var crypto = require('crypto');

var genToken = (payload) => {
	const options = {
		algorithm: 'HS512'
		,expiresIn: (60*60*24)
	};
	return jwt.sign(payload, config.secret, options);
};

var jwtRequiringLogin = function(loginRequired) {
	let x = expJWT({
		secret: config.secret
		,getToken: (req) => {
			let t;
			try {
				t = req.signedCookies[config.tokenName];
			} catch {
				t = null;
			}
			return t;
		}
		,credentialsRequired: loginRequired
	});
	return x;
};

var hashPassword = async (plaintext) => {
	return await bcrypt.hash(plaintext, 12);
};

var comparePassword = async (plaintext, hash) => {
	return await bcrypt.compare(plaintext, hash);
};

var userCookie = (user) => {
	return [config.tokenName
		,genToken({ username: user.username, id: user.id})
		,{httpOnly: true, signed: true}
	];
};

var generateCode = () => {
	//48 bytes to base64 = 64 chars
	return crypto.randomBytes(48).toString('base64');
}

module.exports = {
	requireLogin: jwtRequiringLogin(true)
	,provideUser: jwtRequiringLogin(false)
	,hashPassword: hashPassword
	,comparePassword: comparePassword
	,userCookie: userCookie
	,generateCode: generateCode
}
