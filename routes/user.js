const express = require('express');
const router = express.Router();
const db = require('../db.js');
const auth = require('../auth.js');
const config = require('../config.js');
const utils = require('../utils.js');

router.use(auth.provideUser.unless({path: ['/invite', '/logout', /^\/profile\/\d{1,}$/]}));

router.get('/login', (req, res) => {
	if (req.user) {
		res.redirect('/index');
	} else {
		res.render('login', {message: 'hi there, buddy', user: req.user});
	}
});

router.post('/login', (req, res) => {
	let {username, password} = req.body;
	db.getUserByName(username).then( user => {
		auth.comparePassword(password, user.password).then( same => {
			if (same) {
				res.cookie( ...(auth.userCookie(user)));
				res.redirect('/index');
			} else {
				res.render('login',{message: 'incorrect username/password'});
			}
		})
		.catch( e => {
			console.log('login password check error:', e.stack);
			res.render('login', {message: 'try again later'});
		});
	})
	.catch( e => {
		console.log('login user error', e.stack);
		res.render('login', {message:'no such user'});
	});
});

router.get('/register', (req, res) => {
	res.render('register', {message: 'hi welcome aboard'});
});

router.post('/register', (req, res) => {
	let {username, password, confirmPassword, inviteCode} = req.body;
	db.getCode(inviteCode).then( code => {
		if ((Date.parse(code.expires_at) - Date.now()) < config.inviteTimeout) {
			utils.createUser(username, password).then( successful => {
				if (successful) {
					res.redirect('login');
				} else {
					res.render('register', {message: 'invalid username/password'});
				}
			});
		} else {
			throw 'invalid invite code';
		}
	}).catch( e => {
		res.render('register', {message: e});
	});
});

router.get('/logout', auth.requireLogin, (req, res) => {
	res.clearCookie(config.tokenName);
	res.redirect('login');
});

router.get('/invite', auth.requireLogin, (req, res) => {
	let code = auth.generateCode();
	db.createCode(code, req.user.id).then( successful => {
		res.render('invite', {code: code, user: req.user});
	}).catch( e => {
		console.log('failed to generate invite code', e.stack);
		res.render( 'invite', {message: 'cannot generate invites at this time', user: req.user});
	});
});

router.get('/profile/:id/edit', auth.requireLogin, (req, res) => {
	console.log('profile/id/edit');
	console.log('user', req.user, 'is editing profile', req.params.id);
	console.log('req.user.id !== req.params.id', req.user.id !== Number(req.params.id));
	if (req.user.id !== Number(req.params.id)) {
		console.log('not authorized to edit');
		throw 'not authorized to edit this profile';
	} else {
		console.log('getting user for edit');
		db.getUser(req.params.id).then( user => {
			if (!user) throw 'no such user';
			console.log('loading profile', user.profile);
			res.render('makeProfile', {oldBody: user.profile});
		}).catch( e => {
			console.log('error loading user profile', req.params.id);
			console.log(e);
			res.sendStatus(404);
		});
	}
});

router.post('/profile/:id/edit', auth.requireLogin, (req, res) => {
	if (req.user.id !== Number(req.params.id)) {
		res.status(405).send('you are not user' + req.params.id);
	} else {
		if (req.body.data.body) {
			console.log('req.body.data.body', req.body.data.body);
			db.updateUserProfile(req.user.id, req.body.data.body).then( successful => {
				if	(successful) {
					res.status(200).send({'id': req.params.id});
				} else {
					console.log('failed to update profile', successful);
					res.status(400).send({'message': 'failed to update profile'});
				}
			}).catch( e => {
				console.log('error editing profile', e);
				res.status(500).send({'message': 'error updating profile'});
			});
		} else {
			console.log('bad request');
			res.sendStatus(400);
		}
	}
});

router.get('/profile/:id', auth.provideUser, (req, res) => {
	db.getUser(req.params.id).then( user => {
		if (!user) throw 'no such user';
		let show_controls = (req.user.id === user.id);
		console.log('loading user', user);
		res.render('profile', {
			'show_controls': show_controls,
			'username': user.username,
			'id': user.id,
			'created_at': user.created_at,
			'profile': user.profile,
		});
	}).catch( e => {
		console.log('failed to finder user', req.params.id);
		console.log(e);
		res.status(404);
	});
});

module.exports = router;
