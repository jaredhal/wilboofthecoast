const express = require('express');
const jwt = require('express-jwt');
const router = express.Router();
const config = require('../config');
const auth = require('../auth.js');
const db = require('../db.js');

router.use(auth.requireLogin.unless({path: /^\/post\/\d{1,}$/ }));

router.get('/new', function(req, res) {
	console.log('just checking');
	res.render('makePost', {user: req.user});
});

router.post('/new', function (req, res) {
	console.log('new post!!');
	console.log('new post request', req.body.data);
	if (
		Object.keys(req.body.data).includes('title', 'body')
		&& req.body.data.title !== ''
		&& req.body.data.body !== ''
	) {
		console.log('posting as user', req.user);
		let {title, body} = req.body.data;
		db.createPost(req.user.id, title, body).then( valid => {
			if ( valid ) {
				db.getPostsForUser(req.user.username).then( posts => {
					res.send(200, {id: posts[0].id, user: req.user});
				});
			} else {
				throw 'unable to submit post';
			}
		}).catch( e => {
			console.log('error in new post', e);
			res.sendStatus(500);
		});
	} else {
		console.log('bad request');
		res.sendStatus(400);
	}
})

router.get('/:id', auth.provideUser, function(req, res) {
	db.getPost(req.params.id).then( post => {
		if (post === undefined) {
			res.redirect('/');
		} else {
			let {id, author, user_id, created_at, title, body} = post;
			let authorControls = false;
			if (req.user && req.user.id === user_id) authorControls = true;
			console.log('rendering post', post);
			res.render('viewPost', {
				post: post
				,user: req.user
				,authorControls: authorControls
			});
		}
	}); });

router.get('/:id/edit', (req, res) => {
	db.getPost(req.params.id).then( post => {
		console.log('trying to edit', post.title, 'as user', req.user.id);
		console.log('post body is', post);
		if (post.user_id !== req.user.id) {
			res.status(405).send('you are not author of post ' + req.params.id);
		} else {
			res.render('makePost',
				{user: req.user, oldTitle: post.title, oldBody: post.body}
			);
		}
	})
	.catch( e => {
		console.log('editing post', req.params.id, 'got error:', e.stack);
		res.status(404).send('could not find post ' + req.params.id);
	});
});

router.post('/:id/edit', (req, res) => {
	console.log('edit post request', req.body.data);
	db.getPost(req.params.id).then( post => {
		if(post.user_id !== req.user.id) {
			res.status(405).send('you are not the author of post ' + req.params.id);
		} else {
			if (Object.keys(req.body.data).includes('title', 'body')) {
				let {title, body} = req.body.data;
				db.updatePost(req.params.id, req.user.id, title, body).then( valid => {
					res.send(200, {'id':req.params.id});
				}).catch( e => {
					console.log('error in editing post', e);
					res.sendStatus(500);
				});
			} else {
				console.log('bad request');
				res.sendStatus(400);
			}
		}
	})
});

router.get('/:id/delete', (req, res) => {
	console.log('delete request', req.method);
	db.getPost(req.params.id).then( post => {
		if(post.user_id !== req.user.id) {
			res.send(405, {'error':'you are not the author of post ' + req.params.id});
		} else {
			console.log('deleting post', req.params.id);
			db.deletePost(req.params.id, req.user.id).then( successful => {
				res.redirect('/index');
			}).catch( e => {
				console.log('error editing post', e);
				res.redirect('/post/'+req.params.id);
			});
		}
	});
});

module.exports = router;
