const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const db = require('../db.js');

var home = function(req, res, next) {
	db.getPosts()
		.then( posts => {
			res.render('index' ,{list: posts ,user: req.user});
		}).catch( e => {
			console.log('error in index', e);
			res.sendStatus(404);
		});
};

router.get('/about', auth.provideUser, (req, res) => {
	res.render('about', {user: req.user});
});

router.get('/archive', auth.provideUser, (req, res) => {
	db.getPosts()
		.then( posts => {
			res.render('archive', {posts: posts, user: req.user});
		}).catch( e => {
			console.log('error in archive', e);
			res.sendStatus(404);
		});
});

router.get('/', auth.provideUser, home);
router.get('/index', auth.provideUser, home);

module.exports = router;
