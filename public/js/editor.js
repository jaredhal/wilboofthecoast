// utility function used to inherit non prototypical methods/properties
function extend(target, base) {
	for (var prop in base) {
		target[prop] = base[prop];
	}
}

// definition of a custom Blot.
(function(Embed) {
	'use strict';
	function SVG() {
		Object.getPrototypeOf(Embed).apply(this, arguments);
	}

	SVG.prototype = Object.create(Embed && Embed.prototype);
	SVG.prototype.constructor = SVG;
	extend(SVG, Embed);

	SVG.create = function create(value) { return value };
	SVG.value = function value(domNode) { return domNode };
	SVG.blotName = 'svg';
	SVG.tagName = 'svg';
	SVG.className = 'complex';

	Quill.register(SVG, true);
})(Quill.import('blots/embed')); // import the embed blot. This is important as this is being extended

let imageHandler = function(value) {
	if (value) {
		var range = this.quill.getSelection();
		var value = prompt('enter image URL');
		this.quill.insertEmbed(range.index, 'image', value, Quill.sources.USER);
	}
};

let createManaHandler = (color) => {
	return function() {
		let manaSVG = document.createElement('img');
		//let manaSVG = document.getElementById(`mtg-svg-${color}`).cloneNode(true);
		manaSVG.setAttribute('class', `mtg-svg mtg-svg-${color}`);
		manaSVG.setAttribute('alt', `${color} mana`);
		manaSVG.src = `/images/mtg_${color}.svg`;
		let selection = editor.getSelection();
		this.quill.insertEmbed(selection.index, 'svg', manaSVG);
		this.quill.setSelection(selection.index + 1);
	}
};

const editor = new Quill('#editor', {
	theme: 'snow',
	placeholder: 'write post here...',
	modules: {
		toolbar: {
			container: '#toolbar',
			handlers: {
				'image': imageHandler,
				'whiteMana': createManaHandler('white'),
				'blueMana': createManaHandler('blue'),
				'blackMana': createManaHandler('black'),
				'redMana': createManaHandler('red'),
				'greenMana': createManaHandler('green'),
				'colorlessMana': createManaHandler('colorless')
			},
		}
	}
});
//for inserting cards
const Delta = Quill.import('delta');

// gotta make custom embed blot for SVGs (or a way to add class to image Embed

document.addEventListener('DOMContentLoaded', () => {
	function customButton(selector, color) {
		document.querySelector(selector).addEventListener( 'click', function() {
			var range = editor.getSelection();
			if (range) {
				editor.insertEmbed(range.index, 'image', `../images/mtg_${color}.svg`);
			}
		});
	}
});

function getPostTitle() {
	return document.getElementById('title').value;
}

function getPostBody() {
	return editor.getContents()
}

function submit(url) {
	const post = {
		title: getPostTitle(),
		body: String(editor.root.innerHTML)
	};

	$.ajax({
		method: 'POST',
		url: '/post/new',
		data: JSON.stringify({ data: post }),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: data => {
			console.log(data);
			if (Object.keys(data).includes('id')) {
				window.location.href = '/post/' + data.id;
			} else {
				alert('failed to submit');
			}
		},
		error: err => {
			console.log(err);
		}
	});
}

function newPostSubmit() {
	submit(window.location.href);
}

function editedPostSubmit() {
	submit(window.location.href);
}

function newPostButton() {
	document.getElementById('submit').addEventListener('click', newPostSubmit);
}


function editPostButton() {
	document.getElementById('submit').addEventListener('click', editedPostSubmit);
}

function loadPost(oldTitle, oldBody) {
	//both come in as strings. Parse body to get Quill Delta
	document.getElementById('title').value = oldTitle;
	editor.setContents(oldBody, 'api');
	editor.root.innerHTML = oldBody;
}

function insertCardImage(url) {
	let img = new Delta().insert({image: url});
	let selection = editor.getSelection();
	if (selection) {
		editor.insertEmbed( editor.getSelection().index ,'image' ,url);
	} else {
		editor.updateContents(img);
	}
}

function insertCardText(text) {
	let img = new Delta().insert(text);
	let selection = editor.getSelection();
	if (selection) {
		editor.insertText(selection.index, text);
	} else {
		d = new Delta().insert(text);
		editor.updateContents(d);
	}
}
