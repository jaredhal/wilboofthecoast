const containerId = 'cardFinder';
const cardNameInputId = 'cardName';
const cardSelectContainerId = 'cardSelectContainer';
const cardSelectInputId = 'cardSelect';
const cardSelectButtonId = 'cardSelectButton';
const searchBtnId = 'searchBtn';
const insertBtnId = 'insertBtn';
const noticeId = 'noCardNotice';
const waitElementId = 'waitElement';

document.addEventListener('DOMContentLoaded', () => {
	document.getElementById(searchBtnId).addEventListener('click', () => {
		let cardName = document.getElementById(cardNameInputId).value;
		if (cardName) {
			queryCard(cardName);
		}
	});
});

function queryCard(name) {
	//hit magicthegathering.io

	showWait();

	$.ajax({
		method: 'GET',
		url: 'https://api.magicthegathering.io/v1/cards',
		data: {'name': name},
		error: (e) => {
			hideWait();
			console.log('error', e);
		},
		success: (resp) => {
			hideWait();
			if (resp.cards.length === 0) {
				noCardsFound();
			} else if (resp.cards.length === 1) {
				insertCard(resp.cards[0]);
			} else {
				showList(resp.cards);
			}
		}
	});
};

function noCardsFound() {
	// insert removable notice that no card was found

	let container = document.getElementById(containerId);
	let notice = document.createElement('p');
	notice.setAttribute('id', noticeId);
	notice.innerHTML = 'no card found';
	notice.setAttribute('class', 'card-search card-search-notice');
	container.appendChild(notice);
	document.getElementById(searchBtnId).addEventListener('click', () => {
		notice.remove();
	});
}

function insertCard(cardJSON) {
	console.log(cardJSON);
	// take a formatted card object and insert it into editor
	if (cardJSON.hasOwnProperty('imageUrl')) {
		// from editor file
		insertCardImage(cardJSON.imageUrl);
	} else {
		insertCardText(`${cardJSON.name} : ${cardJSON.text}`);
	}
}

function showList(cards) {
	// insert select element so user can pick card
	// select should call insertCard(card)

	let container = document.getElementById(cardSelectContainerId);
	let select = document.getElementById(cardSelectInputId);
	let button = document.getElementById(cardSelectButtonId);
	container.hidden = false;

	select.innerHTML = null;
	let opt;
	for(let card of cards) {
		opt = document.createElement('option');
		opt.value = JSON.stringify(card);
		if (card.hasOwnProperty('imageUrl')) {
			opt.innerHTML = `${card.name}: ${card.setName}`;
		} else {
			opt.innerHTML = `${card.name}: ${card.setName} (no image)`;
		}
		cardSelect.append(opt);
	}

	button.addEventListener('click', () => {
		let cardJSON = JSON.parse(select.options[select.selectedIndex].value);
		console.log('inserting card', cardJSON);
		insertCard(cardJSON);
		container.hidden = true;
	});
}

function showWait() {
	document.getElementById(waitElementId).hidden = false;
}

function hideWait() {
	document.getElementById(waitElementId).hidden = true;
}
